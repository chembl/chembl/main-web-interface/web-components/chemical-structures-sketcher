module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "prettier",
    "prettier/vue",
    "plugin:vue/recommended",
    "eslint:recommended",
  ],
  parserOptions: {
    parser: "babel-eslint",
  },
  rules: {},
};

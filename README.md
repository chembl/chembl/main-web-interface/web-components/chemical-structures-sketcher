# Chemical Structures Sketcher

This project contains the components used for the chemical structures sketcher in the ChEMBL web apps.

# NPM Repository location

https://www.npmjs.com/package/@chembl/chemical-structures-sketcher

**Note:** The configuration for the packaging and publishing of the components is in the 'library-packager' directory. See the gitlab-ci.yml file to see how the publishing is made.

# Example Usage

You need to setup vuetify in your project before using the components.

An example of usage can be found here:

https://codesandbox.io/s/elastic-danilo-lois1w?file=/src/App.vue

# Marvin JS

This component uses an iframe that generates the sketcher, you need to make the editorws.html avaiable in the static files your your app. For example:

https://wwwdev.ebi.ac.uk/chembl/marvinjs-services/editorws.html

You can pass this path with the parameter *staticMarvinPath*. The path must be within your app's domain otherwise it won't work because of CORS policies. 

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).


### ChemicalSketcherKetcher Component

The `ChemicalSketcherKetcher` component is a Vue.js component that integrates the Ketcher chemical structure editor into your application. It provides a user interface for drawing and editing chemical structures, and it communicates with the Ketcher editor through an iframe.

#### Props

- `eventBus` (Object, required): An event bus object for emitting and listening to events.
- `editorBaseURL` (String, default: `'http://0.0.0.0:8080/ketcher/index.html'`): The URL of the Ketcher editor.

#### Data

- `alert` (Boolean): Controls the visibility of alert messages.
- `alertMessage` (String): The message displayed in the alert.
- `loadingDrawnMol` (Boolean): Indicates whether the drawn molecule is being loaded.
- `ketcherEditor` (Object): Reference to the Ketcher editor instance.
- `editorReady` (Boolean): Indicates whether the editor is ready.
- `currentMol` (String): The current molecule in the editor.
- `errorLoadingEditor` (Boolean): Indicates whether there was an error loading the editor.
- `maxLoadAttempts` (Number): The maximum number of attempts to load the editor.
- `numLoadAttempts` (Number): The current number of attempts to load the editor.

#### Computed Properties

- `alertType` (String): Determines the type of alert to display based on the state of the component.
- `isEditorEmpty` (Boolean): Checks if the editor is empty by analyzing the molecule data.

#### Methods

- `getDrawnMol()`: Retrieves the drawn molecule from the editor and emits an event with the molecule data.
- `insertSDFWhenReady(sdf)`: Inserts an SDF (Structure Data File) into the editor when it is ready.
- `insertSDF(sdf)`: Inserts an SDF into the editor and handles any errors that occur.
- `iframeLoaded()`: Initializes the Ketcher editor when the iframe is loaded.

#### Events

- `error`: Emitted when an error occurs.
- `molObtained`: Emitted when a molecule is successfully obtained from the editor.

#### Example Usage

```vue
<template>
  <ChemicalSketcherKetcher
    :eventBus="eventBus"
    editorBaseURL="http://your-domain.com/ketcher/index.html"
  />
</template>

<script>
import ChemicalSketcherKetcher from './components/ChemicalSketcherKetcher.vue'
import Vue from 'vue'

export default {
  components: {
    ChemicalSketcherKetcher,
  },
  data() {
    return {
      eventBus: new Vue(),
    }
  },
}
</script>
```

This component requires Vuetify for styling and layout. Make sure to set up Vuetify in your project before using this component.